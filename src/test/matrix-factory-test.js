import MatrixFactory from '../app/matrix-factory';
import Constants from './constants';

const testdata = Constants().testdata;

test('csv string in converted into a 2d array', function(){

  const factory = MatrixFactory(',');
 
  const result = factory.getMatrix(testdata).rawMatrix;
  
  expect(result.length).toEqual(4);
  expect(result[0].length).toEqual(7);

});

test('the matrix has zeros replaced', function(){

  const factory = MatrixFactory(',');
 
  const result = factory.getMatrix(testdata).interpolatedMatrix;

  expect(result[0][1].value).toEqual(12.5);

});