import CsvFileReader from '../app/csv-file-reader';
import Constants from './constants';

test('content is returned', function(){

  const testdata = Constants().testdata;

  const blob = new Blob([testdata], {type:'plain/txt'});

  const reader = CsvFileReader(new FileReader());

  reader.getContent(blob).then(text => {expect(text).toEqual(testdata)}); 
});