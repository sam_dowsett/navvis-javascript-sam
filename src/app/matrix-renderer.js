export default function MatrixRenderer(doc){

  function render(matrix){
    const table = doc.createElement('table');
    const body = doc.createElement('tbody');
    matrix.forEach((row)=>{
      body.appendChild(renderTableRow(row));
    })  
    table.appendChild(body);
    return table;
  }

  function renderTableRow(row){
    const tableRow = doc.createElement('tr');
    row.forEach((item) => tableRow.appendChild(renderTableCell(item)));
    return tableRow; 
  }

  function renderTableCell({value, isOriginal}){
    const cell = doc.createElement('td');
    cell.classList.add(isOriginal ? 'original' : "fixed");
    cell.appendChild(doc.createTextNode(value));
    return cell;
  }

  return Object.freeze({
    render
  }); 
}
