import CsvFileReader from './csv-file-reader';
import MatrixFactory from './matrix-factory';
import MatrixRender from './matrix-renderer';
import DomUtils from './dom-utils'

const csvReader = CsvFileReader(new FileReader);
const matrixFactory = MatrixFactory(',');
const renderer = MatrixRender(window.document);
const domUtils = DomUtils();

document.querySelector('#file').addEventListener('change', (event) => {
  csvReader.getContent(event.target.files[0]).then((content) => {
    const raw = window.document.getElementById('target-raw');
    const fixed = window.document.getElementById('target-fixed');
    const proccessedData = matrixFactory.getMatrix(content);
    domUtils.addOrReplaceChildren(raw,renderer.render(proccessedData.rawMatrix));
    domUtils.addOrReplaceChildren(fixed,renderer.render(proccessedData.interpolatedMatrix));
   })
});