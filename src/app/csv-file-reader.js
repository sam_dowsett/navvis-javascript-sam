///Async wrapper for File Reader api
export default function CsvFileReader(fileReader) {
  
  function getContent(file){
    return new Promise((resolve, reject) => {
      fileReader.onerror = () => {
        fileReader.abort();
        reject(new Exception("cant read file"));
      }

      fileReader.onload = () => {
        resolve(fileReader.result);
      }

      fileReader.readAsText(file);
    }) 
  }


  return Object.freeze({
    getContent
  });
} 