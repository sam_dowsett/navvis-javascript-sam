export default function MatrixFactory(delimiter){
  const newline  = '\n';

   class Neighbors {
    constructor(north, south, east, west){
      this.north = north;
      this.south = south;
      this.east = east;
      this.west = west;
      }

     average(){
      return (this.north + this.south + this.east + this.west) / 4;   
     } 
  }
  
  function getMatrix(csvContent){
    const rawMatrix =  csvContent.split(newline).map((row) => { 
      return row.split(delimiter).map((cell) => {return {value:parseInt(cell), isOriginal:true}});
    })

    return interpolateZeroValues({rawMatrix: rawMatrix});
  } 

  function interpolateZeroValues({rawMatrix}){

    const interpolatedMatrix = new Array(rawMatrix.length);

    rawMatrix.forEach((row, yIndex, array) => {
      const newRow = new Array(row.length);
      row.forEach((item, xIndex) => {
          newRow[xIndex] = getAverageValueOfNeighbours(item.value, xIndex, yIndex, array);
      })
      interpolatedMatrix[yIndex] = newRow 
    })

    return {rawMatrix, interpolatedMatrix: interpolatedMatrix}; 
  }

  function getAverageValueOfNeighbours(originalValue, xIndex, yIndex, matrix){
       if (originalValue !== 0){
          return {value: originalValue, isOriginal:true}; 
         } else {
          return seekNonZeroValuesFrom(xIndex, yIndex, matrix);
        }
  }

  function seekNonZeroValuesFrom(xIndex, yIndex, matrix){

    var nearestNeighbors = new Neighbors( yIndex-1 >= 0 ? matrix[yIndex-1][xIndex].value : 0 
      ,yIndex+1 < matrix.length ? matrix[yIndex+1][xIndex].value : 0
      ,xIndex-1 >= 0 ? matrix[yIndex][xIndex-1].value : 0
      ,xIndex+1 < matrix[yIndex].length ? matrix[yIndex][xIndex+1].value : 0);
  
    return {value: nearestNeighbors.average(), isOriginal:false};
  }
  
  return Object.freeze({
    getMatrix
  });
} 