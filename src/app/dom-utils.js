export default function DomUtils(){
  function addOrReplaceChildren(target, newElement){
    if(target.children[0]) {
        target.replaceChild(newElement, target.children[0]);
    } else{
      target.appendChild(newElement);
    }
  }

  return Object.freeze({
    addOrReplaceChildren
  });
}